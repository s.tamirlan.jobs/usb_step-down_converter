# USB Step down converter

The function of the PCB is to convert the voltage from 5 V to 3.3 V.

Since then, due to the fact that the project was not maintended and not developed further, it was not tested and implemented in the form of a printed circuit board. However it is possible to materialize it.

You are welcome to review and help improve this PCB project. Also you can reach me out via [my contacts](#contacts)

## Information
---


* Max Input: Input withstands Up to 20 V (according to [Data Sheet][1])
* Max Output: 1.5A (according to [Data Sheet][2])

It's meant to be compatible with both **USB Type A** or **USB 3.0**

Software: **Altium Designer 19**

## Circuit board
---

![Circuit board](./images/USB_step_down_converter.png)


## The schematic
---
*This schematic diagram was created specifically for convenient use.

![Schematic](./images/USB_converter.png)


# Contacts

EMail : <s.tamirlan.jobs@gmail.com>  
Linkedin : <https://www.linkedin.com/in/stamirlan>


[1]: https://www.ti.com/lit/gpn/tps25200
[2]: https://www.ti.com/lit/gpn/tps786